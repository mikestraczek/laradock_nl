<?php namespace NextLevels\Docker\Commands;

use Symfony\Component\Console\Command\Command;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Class BaseCommand
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class BaseCommand extends Command
{
    protected $twig;

    /**
     * Return the twig instance
     *
     * @return Environment
     */
    protected function getTwig(): Environment
    {
        if (! $this->twig) {
            $loader = new FilesystemLoader($this->getSkeletonBaseDirectory());
            $this->twig = new Environment($loader);
        }

        return $this->twig;
    }

    /**
     * Returns the skeleton base path
     *
     * @return string
     */
    protected function getSkeletonBaseDirectory(): string
    {
        $splitClass = explode('\\', static::class);
        $generatorClass = end($splitClass);

        return realpath(__DIR__ . '/' . $generatorClass) . DIRECTORY_SEPARATOR;
    }
}
