<?php namespace NextLevels\Docker\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

/**
 * Class GenerateConfigCommand
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class GenerateConfigCommand extends BaseCommand
{

    /**
     * Configure command
     */
    protected function configure(): void
    {
        $this
            ->setName('make')
            ->setDescription('Generate new docker-compose.yml file')
            ->setHelp('This command generate docker-compose.yml file with given name if no exists')
            ->addArgument('name', InputArgument::REQUIRED, 'Project name');
    }

    /**
     * Execute generate config command
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $invalidCharCount = 0;
        $name = preg_replace('/\W/', '_', trim($input->getArgument('name')), -1, $invalidCharCount);
        $fileName = 'docker-compose.yml';
        $io = new SymfonyStyle($input, $output);

        $io->title(sprintf('Generate %s file', $fileName));

        if ($invalidCharCount > 0) {
            throw new \RuntimeException(
                'Project name should only contain letters (a-z, A-Z), numbers (0-9) and underscores.'
            );
        }

        if (file_exists($fileName)) {
            if ($io->confirm($fileName . ' file already exists, you want to update the file? (Settings will be reset!)', false)) {
                $currentValues = Yaml::parseFile($fileName);
                $newValues = Yaml::parse($this->getTwig()->render($fileName . '.twig', ['name' => $name]));

                file_put_contents(
                    $fileName,
                    Yaml::dump(array_merge($currentValues, $newValues), 5)
                );

                $io->success($fileName . ' file updated!');
            }

            return;
        }

        file_put_contents(
            $fileName,
            $this->getTwig()->render($fileName . '.twig', ['name' => $name])
        );

        $io->success($fileName . ' file generated!');
    }
}
